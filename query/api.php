<?php
require_once("jsonRPCClient.php");
require_once("config.php");
$niobio = new Niobio($rpc_user, $rpc_pass, $rpc_host, $rpc_port);
$saldo = $niobio->getbalance();
$erro = $niobio->error;

$printavel["result"] = $saldo;
$printavel["error"] = $erro;

echo json_encode($printavel, true);

?>
